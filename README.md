[digitalPRAXIS.de](https://digitalpraxis.de/)
Portfolio webpage by Robert Karpinski
----
![screenshot](https://digitalpraxis.de/img/screenshot.jpg)

**Visit the [LIVEPAGE on digitalpraxis.de ](https://digitalpraxis.de/)**

My personal freelancing Portfolio was build using the cool tools vue-cli3 and vuetify.js 
For more insight see:
- https://vuejs.org/
- https://cli.vuejs.org/
- https://vuetifyjs.com/



### npm

Install via npm:

```bash
git clone git@gitlab.com:riskieee/vue-digitalpraxis.git

npm install

npm run serve

npm run build
```