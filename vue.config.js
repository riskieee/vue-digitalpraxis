// vue.config.js
// ignored but should make v-img work in webpack img url compile...
module.exports = {
  baseUrl: process.env.NODE_ENV === 'production' ?
    '/' :
    './',
  configureWebpack: {
    devtool: 'source-map'
  },
  chainWebpack: config => {
    config.module
      .rule('vue')
      .use('vue-loader')
      .loader('vue-loader')
      .tap(options => Object.assign(options, {
        transformAssetUrls: {
          'v-img': ['src', 'lazy-src'],
          'v-card': 'src',
          'v-card-media': 'src',
          'v-responsive': 'src'
        }
      }))
  }
}