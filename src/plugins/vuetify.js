import Vue from 'vue'
import {
  Vuetify,
  VApp,
  VNavigationDrawer,
  VDivider,
  VImg,
  VHover,
  VCard,
  VFooter,
  VList,
  VBtn,
  VIcon,
  VGrid,
  VToolbar,
  VTimeline,
  VTabs,
  VStepper,
  VParallax,
  transitions,
  VCarousel
} from 'vuetify'
import 'vuetify/src/stylus/app.styl'

Vue.use(Vuetify, {
  components: {
    Vuetify,
    VApp,
    VNavigationDrawer,
    VDivider,
    VImg,
    VHover,
    VCard,
    VFooter,
    VList,
    VBtn,
    VIcon,
    VGrid,
    VToolbar,
    VTimeline,
    VTabs,
    VStepper,
    VParallax,
    transitions,
    VCarousel
  },
  theme: {
    white: '#FFFFFF',
    black: '#000000',
    primary: '#DD0000',
    secondary: '#E0E0E0',
    accent: '#B71C1C',
    error: '#E91E63',
    warning: '#FBC02D',
    info: '#00BFA5',
    success: '#43A047'
  }
})
