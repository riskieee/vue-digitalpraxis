import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    // index default home as fallback withpit lazy-loading!
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/service',
      name: 'service',
      component: () => import(/* webpackChunkName: "service" */ './views/Service.vue')
    },
    {
      path: '/projects',
      name: 'projects',
      component: () => import(/* webpackChunkName: "projects" */ './views/Projects.vue')
    },
    {
      path: '/profile',
      name: 'profile',
      component: () => import(/* webpackChunkName: "profile" */ './views/Profile.vue')
    },
    {
      path: '/contact',
      name: 'contact',
      component: () => import(/* webpackChunkName: "contact" */ './views/Contact.vue')
    },
    {
      path: '/legal', // fake URL browser path
      name: 'legal', // alias for vue
      // route level code-splitting for lazy-loaded when the route is visited
      // this generates a separate chunk (about.[hash].js) for this route
      component: () => import(/* webpackChunkName: "legal" */ './views/Legal.vue') // real src folder path
    }
  ]
})
